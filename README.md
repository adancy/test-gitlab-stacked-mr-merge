This repo contains a program to exercise Gitlab's ability to support
automation on stacked merge requests.  This program triggers the
issues described in [#441446](https://gitlab.com/gitlab-org/gitlab/-/issues/441446) (and probably other tickets).

To setup:

```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Edit `main.py` and change the value of `VICTIM_REPO` to point to a
repo that you have write access to.

Set `GITLAB_PRIVATE_TOKEN` in your environment.  It must hold a Gitlab
token with api access for `VICTIM_REPO`.

To run one round of exercise:
```
python3 main.py
```
to perform one round of exercise.

To run the exerciser in a loop until it fails:
```
while python3 main.py; do :; done
```
