#!/usr/bin/env python

import git
import gitlab
import os
import tempfile
import time

VICTIM_REPO = "adancy/test-victim"

def get_sequence_number() -> int:
    last_seq = 0
    try: 
        with open("sequence") as f:
            last_seq = int(f.read())
    except:
        pass

    seq = last_seq + 1

    with open("sequence", "w") as f:
        f.write(str(seq))

    return seq
        

def wait_until_mergeable(project, mr):
    start = time.time()
    timeout = 120

    print(f"Waiting for {mr.web_url} to become mergeable...")

    while time.time() - start < timeout:
        # Refresh
        mr = project.mergerequests.get(mr.iid)
        print(f"detailed_merge_status: {mr.detailed_merge_status}")
        if mr.detailed_merge_status == 'mergeable':
            print("Ready!")
            return mr
        
        time.sleep(1)

    raise SystemExit(f"{mr.web_url} did not become mergeable within {timeout} seconds")


def run_test(project):
    with tempfile.TemporaryDirectory() as repodir:
        victim_url = f"git@gitlab.com:{VICTIM_REPO}.git"
        print(f"Cloning {victim_url}")
        repo = git.Repo.clone_from(victim_url, repodir)
        target_branch = f"test-{get_sequence_number()}"

        print(f"Creating test branch: {target_branch}")
        branch_ref = repo.active_branch.name
        project.branches.create({"branch": target_branch,
                                 "ref": branch_ref})
        repo.create_head(target_branch).set_tracking_branch(repo.remotes.origin.refs[branch_ref]).checkout()

        print("Creating test commits")

        commits = []
        pushes = []

        for i in range(0,3):
            filename = f"testfile{i}"
            filepath = os.path.join(repodir, filename)
            open(filepath, "w").close()
            repo.index.add([filepath])
            commit = repo.index.commit(f"Add {filename}")
            commits.append(commit)
            commit_branch = f"{target_branch}-commit{i}"
            pushes.append(f"{commit.hexsha}:refs/heads/{commit_branch}")

        print("Pushing commits")
        repo.remotes.origin.push(pushes).raise_if_error()
        
        print("Creating MRs")
        mrs = []
        current_target = target_branch
        for i, commit in enumerate(commits):
            commit_branch = f"{target_branch}-commit{i}"
            mr = project.mergerequests.create({"source_branch": commit_branch,
                                               "target_branch": current_target,
                                               "title": commit.message.splitlines()[0],
                                               "remove_source_branch": True,
                                               })
            mrs.append(mr)
            print(mr.web_url)
            current_target = commit_branch

        print("\nMerging MRs")
        for mr in mrs:
            while True:
                mr = wait_until_mergeable(project, mr)
                print(f"Merging {mr.web_url}")
                try:
                    merge_res = mr.merge()
                    break
                except Exception as e:
                    # Cripes
                    print(e)
                    print(f"Merge failed even though MR status was mergeable.  Lies!!")
                    print("Trying again in 1 second...")
                    time.sleep(1)
            print("Merged!\n")
            changes_count = merge_res["changes_count"]
            if changes_count is None or int(changes_count) != 1:
                raise SystemExit(f"Expected changes_count of {mr.web_url} to be 1, but it is {changes_count}")
            

def main():
    token = os.getenv("GITLAB_PRIVATE_TOKEN")
    if not token:
        raise SystemExit("Please set GITLAB_PRIVATE_TOKEN in the environment")
    
    gl = gitlab.Gitlab("https://gitlab.com", private_token=token)
    project = gl.projects.get(VICTIM_REPO)

    run_test(project)
    

if __name__ == "__main__":
    main()
    
    

    
